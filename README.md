# bai-oe-build

A docker image used for jetson-distro builds

## Building the Image

This project utilizes [Rake](https://github.com/ruby/rake).  List availabile commands by executing `rake -T`:

```
adam@malak:~/bai-oe-build$ rake -T
rake image                     # Build the bai-oe-build:0.0.1 docker image
rake root_shell                # Open a root-user shell into the bai-oe-build image
rake set_version[new_version]  # Set the version/tag of the images built by this rakefile eg rake set_version[0.0.1-dev]
rake shell                     # Open a shell into the bai-oe-build:0.0.1 image as oe-user
```

Set the current version of the build by executing `rake set_version[0.0.2]`.

Build the image by executing `rake image`

## Build/Deploy Pipeline
This project uses Bitbucket Pipelines to build/deploy to [Docker Hub](https://hub.docker.com/repository/docker/baiadam/bai-oe-build)

Currently, the pipeline is triggered by the `master` branch and tags formatted as [`release-$SEMANTIC_VERSION`](https://semver.org/)

When the `master` branch is updated, that branch is built, then pushed to docker hub tagged both as the commit hash and "master".  This can be modified to accept any branch, which would allow us to easily pull the latest successful build of any given branch for testing.

When a tag of the format `release-*.*.*` is pushed to the repo, that tag will be built, then push to docker hub tagged both as the version (e.g. `0.0.1`) and `latest`.
## Usage

Bring up a bash prompt in the image by executing

```
JETSON_DISTRO_DIRECTORY=~/jetson-distro
docker run --rm -v ${JETSON_DISTRO_DIRECTORY}:/app/oe -v ~/.ssh:/home/oe-user/.ssh --user oe-user -it bai-oe-build:0.0.1 /bin/bash
```

Then source/bitbake your build as you normally would