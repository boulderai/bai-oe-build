RAKEFILE_DIR = File.expand_path(File.dirname(__FILE__))
DOCKER_IMAGE = "bai-oe-build"
USERNAME = "oe-user"
VERSION_FILE = File.join(RAKEFILE_DIR, ".version")

def image_version
    if File.file?(VERSION_FILE)
        return File.read(VERSION_FILE).split[0]
    end
    return "dev-local"
end

desc "Set the version/tag of the images built by this rakefile eg rake set_version[0.0.1-dev]"
task :set_version, [:new_version] do |t, args|
    File.write(VERSION_FILE, args[:new_version])
end

desc "Build the #{DOCKER_IMAGE}:#{image_version} docker image"
task :image do
  sh "docker build -t #{DOCKER_IMAGE}:#{image_version} #{RAKEFILE_DIR}"
end

desc "Create a tarball of the the #{DOCKER_IMAGE}:#{image_version} docker image at #{DOCKER_IMAGE}-#{image_version}.tar.gz"
task :tarball => [:image] do
  sh "docker save #{DOCKER_IMAGE}:#{image_version} | gzip > #{DOCKER_IMAGE}-#{image_version}.tar.gz"
end

desc "Open a root-user shell into the #{DOCKER_IMAGE} image"
task :root_shell => [:image] do
  sh "docker run -it --privileged #{DOCKER_IMAGE}:#{image_version} /bin/bash"
end

desc "Open a shell into the #{DOCKER_IMAGE}:#{image_version} image as #{USERNAME}"
task :shell => [:image] do
  sh "docker run --user #{USERNAME} -it --privileged #{DOCKER_IMAGE}:#{image_version} /bin/bash"
end
