FROM ubuntu:focal

WORKDIR /app

# ** Variables **

ENV USERNAME=oe-user

# ** Provisioning the container **

RUN apt-get update
RUN apt-get install -y apt-utils

# Create the non-root user
RUN useradd $USERNAME
RUN mkdir -p /home/$USERNAME
RUN cp /etc/skel/.bashrc /home/$USERNAME/.

# Install sudo and give non-root user access
RUN apt-get install -y sudo && rm -rf /var/lib/apt/lists/*
RUN usermod -aG sudo $USERNAME
RUN echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN chown $USERNAME:$USERNAME /home/$USERNAME -R

# Replace bash as /bin/sh
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Copy packages.txt into the container
ADD packages.txt /app

# Update the package listing and install the packages in packages.txt
RUN dpkg --add-architecture i386
RUN apt-get update
RUN apt-get install -y locales
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y $(grep -vE "^\s*#" /app/packages.txt  | tr "\n" " ")

ADD pip-requirements.txt /app
RUN pip3 install -r pip-requirements.txt

# Language options

# Set up utf8
RUN locale-gen en_US.UTF-8
RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8# Set up utf8

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

RUN mkdir -p /app/oe && chown -R oe-user:oe-user /app/oe

USER oe-user
ENV USER=oe-user
WORKDIR /app/oe